<?php
/**
 * The page template
 *
 * @package WordPress
 * @subpackage SKEL-ETOR
 * @since SKEL-ETOR 1.0
 */
?>

<?php 
	$args = array(
		'numberposts' => 1,
		'post_type' => 'post',
		);

    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );

	if(isset($recent_posts) && !empty($recent_posts)) {
		
		$gallery = '';
		foreach(get_field("field_5625e81fdeceb",$recent_posts[0]['ID']) as $image) {
			$gallery .= '<img src="'.$image['url'].'"/>';
		}
		
		$address = get_field("field_56278c555e39d",$recent_posts[0]['ID']);
		
		
		$link_list = '<ul>';
		foreach(get_field("field_5625f1062c3ab",$recent_posts[0]['ID']) as $link) {
			$link_list .= '<li>'.$link['link'].'</li>';
		}
		$link_list .= '</ul>';
		
		$tags = wp_get_post_tags($recent_posts[0]['ID']);
		
		$tags_list = '<ul>';
		foreach($tags as $tag) {
			$tags_list .= '<li>'.$tag->name.'</li>';
		}
		$tags_list .= '</ul>';
	}
?>

<article id="post-<?php echo get_the_ID(); ?>" <?php post_class() ?>>
	<header>
		<h1><?php the_title(); ?></h1>
	</header>
	<div class="entry row">
		<div class="col-xs-12 col-md-6 col-sm-12 indent">
			<?php the_content(); ?>
		</div>
		
		<div class="col-xs-12 col-md-6 col-sm-12 indent prev_bg">
		<h2 class="prev_title">Preview</h2>
			<div id="preview">
				<div class="prev_head"><h2><?php echo isset($recent_posts[0]['post_title'])?$recent_posts[0]['post_title']:""; ?></h2></div>
				<div class="prev_body">
					<div class="post_content post_field"><h3>content</h3><?php echo isset($recent_posts[0]['post_title'])?$recent_posts[0]['post_content']:""; ?></div>
					<div class="post_image post_field"><h3>image</h3><?php echo isset($recent_posts[0]['post_title']) ? get_the_post_thumbnail($recent_posts[0]['ID'], 'thumbnail') : "123"; ?></div>
					<div class="post_tag post_field"><h3>tag</h3><?php echo isset($recent_posts[0]['post_title'])?$tags_list:""; ?></div>
					<div class="post_address post_field"><h3>address</h3><?php echo isset($recent_posts[0]['post_title'])?$address:""; ?></div>
					<div class="post_gallery post_field"><h3>gallery</h3><?php echo isset($recent_posts[0]['post_title'])? $gallery :""; ?></div>
					<div class="post_url_list post_field"><h3>url_list</h3><?php echo isset($recent_posts[0]['post_title'])?$link_list:""; ?></div>
				</div>
				
			</div>
		</div>
	</div>
	<footer>
		<?php do_action('skel_etor_page_footer'); ?>
	</footer>
</article>