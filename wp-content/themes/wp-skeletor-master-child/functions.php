<?php
function theme_name_scripts(){
	wp_register_style('style_my', get_stylesheet_directory_uri() . '/css/my.css');
	wp_enqueue_style('style_my');



	/* bootstrap material */	
	
	wp_register_style('css1', get_stylesheet_directory_uri() . '/lib/bootstrap_material/css/bootstrap.min.css');
	wp_enqueue_style('css1');
	
	wp_register_style('css2', get_stylesheet_directory_uri() . '/lib/bootstrap_material/css/main.css');
	wp_enqueue_style('css2');

	
	wp_register_script( 'script1', get_stylesheet_directory_uri() . '/lib/bootstrap_material/js/bootstrap.min.js');
	wp_enqueue_script( 'script1' );
	
	wp_register_script( 'script6', get_stylesheet_directory_uri() . '/js/my.js');
	wp_enqueue_script( 'script6' );


}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts',100  );