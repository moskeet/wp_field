<script type="text/javascript">
function StartAddCustomField(label, type, index) {
    if (!CanFieldBeAdded(type)) {
    jQuery('#gform_adding_field_spinner').remove();
    return;
    }

    if (gf_vars["currentlyAddingField"] == true)
    return;

    gf_vars["currentlyAddingField"] = true;

    var nextId = GetNextFieldId();
    var field = CreateCustomField( nextId, type, label, index );

    var mysack = new sack("<?php echo admin_url( 'admin-ajax.php' )?>?id=" + form.id);
    mysack.execute = 1;
    mysack.method = 'POST';
    mysack.setVar("action", "rg_add_field");
    mysack.setVar("rg_add_field", "<?php echo wp_create_nonce( 'rg_add_field' ) ?>");
    mysack.setVar("index", index);
    mysack.setVar("field", jQuery.toJSON(field));
    mysack.onError = function () {
    alert(<?php echo json_encode( esc_html__( 'Ajax error while adding field', 'gravityforms' ) ); ?>)
    };
    mysack.runAJAX();

    return true;
}

function CreateCustomField( id, type, label, index ) {
    var field = new CustomField(id, label);
    CustomSetDefaultValues( field, type, index );

    if (field.type == "captcha") {
        <?php
        $publickey = get_option( 'rg_gforms_captcha_public_key' );
        $privatekey = get_option( 'rg_gforms_captcha_private_key' );
        if ( class_exists( 'ReallySimpleCaptcha' ) && ( empty( $publickey ) || empty( $privatekey ) ) ){
            ?>
        field.captchaType = "simple_captcha";
        <?php
    }
    ?>
    }
    return field;
}

function CustomField(id, label){
    this.id = id;
    this.label = label;
    this.adminLabel = "";
    this.type = 'ACF_field';
//    this.isRequired = false;
    this.size = "medium";
    this.errorMessage = "";
    //NOTE: other properties will be added dynamically using associative array syntax
}

function CustomSetDefaultValues( field, type, index ) {
console.log(field);
    var inputType = type == 'gallery' ? 'fileupload' : type;
    field.inputs = null;
    field.choices = '';
    field.displayAllCategories = true;
//    field.inputType = inputType;


    if (window["SetDefaultValues_" + inputType])
        field = window["SetDefaultValues_" + inputType](field);
}
</script>