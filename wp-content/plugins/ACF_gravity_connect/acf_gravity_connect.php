<?php

/**
 * Plugin Name: ACF Gravity Connect
 * Plugin URI:
 * Description: Cloning posts and pages in WordPress.
 * Version: 1.0
 * Author: Lukasz Kostrzewa
 * Author URI:
 * License: GPL2
 */

class acf_gravity_connect {

    function __construct() {
        add_action('admin_enqueue_scripts', array(&$this, 'custom_files'));
        add_filter('gform_add_field_buttons', array(&$this, 'custom_button'), 1);
        add_action('gform_after_save_form', array(&$this, 'fill_field_settings'), 10, 2);
        add_action( 'gform_post_submission', array(&$this, 'set_post_content'), 10, 2 );
        add_filter( 'gform_field_validation', array(&$this, 'custom_validation'), 10, 4 );
    }

    /*Vlidate address*/
    function custom_validation($result, $value, $form, $field) {

        if($field->postCustomFieldName == 'address') {
            if(preg_match('/(\(|\)|\�|\,|\%|\#)/', $value)) {
                $result['is_valid'] = false;
                $result['message'] = 'Invalid address';
            } else {
                $result['is_valid'] = true;
            }
        }

        return $result;
    }

    /*Save gallery images to ACF field*/
    function set_post_content($entry, $form) {
//        var_dump($entry, $form);
        foreach($form['fields'] as $key=> $field) {
            if($field->type == 'post_custom_field' && $field->inputType == 'fileupload') {
                $field_num = $field->id;
                $values = json_decode($entry[$field_num]);

                $wp_upload_dir = wp_upload_dir();
                $gallery = array();
                foreach ($values as $image) {
                    $image_path = $wp_upload_dir['basedir'].preg_replace("/http.*?uploads\//", '', $image);
                    $wp_filetype = wp_check_filetype($image_path, null );
                    $attachment = array(
                        'guid'           => $image,
                        'post_mime_type' => $wp_filetype['type'],
                        'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $image_path ) ),
                        'post_content'   => '',
                        'post_status'    => 'inherit'
                    );
                    $attach_id = wp_insert_attachment( $attachment, $image, $entry['post_id'] );

                    $gallery[] = $attach_id;

                    require_once( ABSPATH . 'wp-admin/includes/image.php' );

                    $attach_data = wp_generate_attachment_metadata( $attach_id, $image_path );
                    wp_update_attachment_metadata( $attach_id, $attach_data );
                }

                delete_post_meta($entry['post_id'], $field->postCustomFieldName);
                update_post_meta($entry['post_id'], $field->postCustomFieldName, $gallery);
                update_post_meta($entry['post_id'], '_'.$field->postCustomFieldName, $field->ACF_key);

            } else if($field->type == 'post_custom_field' && $field->inputType == 'list') {
                $field_num = $field->id;
                $values = unserialize($entry[$field_num]);
//                var_dump($field, $values);

                update_post_meta($entry['post_id'], $field->postCustomFieldName, count($values));
                update_post_meta($entry['post_id'], '_'.$field->postCustomFieldName, $field->choices[0]['ACF_key']);
                foreach($values as $key=> $value) {
                    update_post_meta($entry['post_id'], $field->postCustomFieldName.'_'.$key.'_'.str_replace(' ', '_', $field->choices[0]['text']) , $value);
                    update_post_meta($entry['post_id'], '_'.$field->postCustomFieldName.'_'.$key.'_'.$field->choices[0]['text'] , $field->ACF_key);
                }

            }
        }
    }

    /*Save ACF field settings to Gravity form field*/
    function fill_field_settings($form) {

        foreach($form['fields'] as $field) {
            if($field->type == 'ACF_field') {
                $acf_field = get_field_object($field->label);
                $field->type = 'post_custom_field';
                $field->postCustomFieldName = $acf_field['name'];
                $field->isRequired = $acf_field['required'] == 1 ? true : false;
                $field->ACF_key = $field->label;
                $field->cssClass = $field->label;
                if($acf_field['type'] == 'gallery') {
                    $field->inputType = 'fileupload';
                    $field->multipleFiles = true;
                    $field->maxFiles = $acf_field['max'];
                    $field->maxFileSize = $acf_field['max_size'];
                    $field->allowedExtensions = $acf_field['mime_types'];
                } else if($acf_field['type'] == 'repeater') {
                    $field->inputType = 'list';
                    $field->choices = array();

                    foreach($acf_field['sub_fields'] as $subfield) {
                        $field->choices[] = array(
                            'text' => $subfield['label'],
                            'value' => $subfield['name'],
                            'ACF_key' => $subfield['key'],
                            'isSelected' => false,
                            'price' => ''
                        );
                    }
                }
//var_dump($acf_field);

                $field->label = $acf_field['label'];
            }
//            var_dump($field->choices);

        }
        GFAPI::update_form( $form );
        GFAPI::update_forms_property( $form['id'], 'is_active', 1 );

    }

    /*Add ACF fields to Gravity form*/
    function custom_button($field_groups) {

        $custom_fields = get_posts(array('post_type' => 'acf-field'));

        foreach ($field_groups as $key => &$group) {
            if ($group['name'] == 'post_fields') {
                foreach ($custom_fields as $custom_field) {
                    $field = unserialize($custom_field->post_content);
                    $field['type'] = $field['type'] == 'gallery' ? 'fileupload' : $field['type'];
                    $group['fields'][] = array(
                        'class' => 'button',
                        'date-type' => 'ACF_field',
                        'value' => $custom_field->post_title,
                        'onclick' => "StartAddCustomField('" . $custom_field->post_name . "', 'ACF_field');"
                    );
                }
            }

        }
        return $field_groups;
    }

    function custom_files() {
        include_once(plugin_dir_path(__FILE__).'/assets/main.php');
    }
}

$acf_gravity_connect = new acf_gravity_connect();