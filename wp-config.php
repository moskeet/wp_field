<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'skelet');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U+LtFy]#-0,:~EKVd1~T1-7il*)TkXu}ZxUtQ<z(7FtjT+qAgCbHlp6{)pO!|h{_');
define('SECURE_AUTH_KEY',  'Mg=oav},M^n2t*mK|At>}VQAG8k9K!m`1:47Al<{!iSSyTQ?+9kAFYa7pGI+,I/l');
define('LOGGED_IN_KEY',    '!=esKES||OmPCBn^|?ovm-c7i|Rjb/X,[!?I5G?l^`&ZE2xU Q#kWSt!C@+BNCh<');
define('NONCE_KEY',        'TR]wX&/<_lpdZgZ@-gnA@i~VJKwwjt#0AAlNufiLr#^n~KYo$c+}iI|[#WT+5jFs');
define('AUTH_SALT',        'U>2uJthFI0kDBl+_ko+vh.D6j@4%5w-,<SaC{G9pJ_7l_WuOW,&{yXlgL8D`jRKg');
define('SECURE_AUTH_SALT', 'T`F@?TklN5(@-y||a$=Cn(q>akf{W3s?iSqRI~:A!8czq|Qb~)VV8+04t?2 =:Z#');
define('LOGGED_IN_SALT',   'Xww(O]:#fq1{PzpM}xg6opGP6_7M4[Gv~E:SgX!I_-=(sV.^eC43<ua{QXw,B+m2');
define('NONCE_SALT',       'yI#>b5$UFf lr#n(:rrvhIGtyz;FCW+tNNWTiJos++xBVk&qw<8kLQu(R|~nMy@9');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
